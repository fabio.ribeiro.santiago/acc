package fabio;

import graphs.DepthFirstOrder;
import graphs.Digraph;
import graphs.In;
import graphs.StdOut;

public class Task1 {
	
	public void run() {

		// In in = new In(args[0]);
		long startTime, totalTime;
		String filename = System.getProperty("user.dir") + "/doc/mediumDG.txt";		
		
		In in = new In(filename);
		Digraph G = new Digraph(in);
		
		
		startTime = System.currentTimeMillis();
		DepthFirstOrder dfs = new DepthFirstOrder(G);
		totalTime = System.currentTimeMillis() - startTime;

		StdOut.print("Preorder:  ");
		for (int v : dfs.pre()) {
			StdOut.print(v + " ");
		}
		StdOut.println();

		StdOut.print("Postorder: ");
		for (int v : dfs.post()) {
			StdOut.print(v + " ");
		}
		StdOut.println();
		
		System.out.println("Graph file used: " + filename);
		System.out.println("Algorithm used: DepthFirstOrder");
		System.out.println("Number of vertices: " + G.V());
		System.out.println("Number of edges: " + G.E());
		System.out.println("Complexity for graph creation in worst case: Proportional to O(|V| + |E|)" );
		System.out.println("Complexity to print pre-order and post-order: Proportional to O(|V|)" );
		System.out.println("CPU time to run DSF: " + totalTime + " miliseconds");
		
	}
}
