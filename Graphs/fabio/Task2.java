package fabio;

import graphs.DijkstraSP;
import graphs.DirectedEdge;
import graphs.Edge;
import graphs.EdgeWeightedDigraph;
import graphs.EdgeWeightedGraph;
import graphs.In;
import graphs.KruskalMST;
import graphs.StdOut;

public class Task2 {
	
	private String filename = System.getProperty("user.dir") + "/doc/mediumEWG.txt";

	private void a() {
		
		System.out.println("Running task2 item a...");
		In in = new In(this.filename);
		EdgeWeightedDigraph G = new EdgeWeightedDigraph(in);
		long startTime, totalTime;
		DijkstraSP sp;

		startTime = System.currentTimeMillis();
		for (int origin=0; origin < G.V(); origin++) {
			sp = new DijkstraSP(G, origin);
		}
		totalTime = System.currentTimeMillis() - startTime;
		
		System.out.println("Graph file used: " + this.filename);
		System.out.println("Algorithm used: Dijkstra with a binary heap");
		System.out.println("Number of vertices: " + G.V());
		System.out.println("Number of edges: " + G.E());
		System.out.println("Complexity of Shortest Path for one vertice: O(|E| log |V|)" );
		System.out.println("Complexity for task2a: O(|V|.(|E| log |V|))" );
		System.out.println("CPU time to generate all vertices shortests path: " + totalTime + " miliseconds");
	}
	
	private void b() {
		System.out.println("Running task2 item b...");

		long startTime, totalTime;
		In in = new In(this.filename);
        EdgeWeightedGraph G = new EdgeWeightedGraph(in);
        
        startTime = System.currentTimeMillis();
        KruskalMST mst = new KruskalMST(G);
		totalTime = System.currentTimeMillis() - startTime;
		
		System.out.println("Graph file used: " + this.filename);
		System.out.println("Algorithm used: Kruskal");
		System.out.println("Number of vertices: " + G.V());
		System.out.println("Number of edges: " + G.E());
		System.out.println("Complexity for task2b: O(|E| log |V|)" );
		System.out.println("CPU time to generate minimum spanning tree: " + totalTime + " miliseconds");

	}
	
	public void run() {
		this.a();
		System.out.println("");
		this.b();
	}

}
