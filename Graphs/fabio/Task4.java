package fabio;

import java.util.ArrayList;
import java.util.List;

import graphs.CC;
import graphs.Graph;
import graphs.StdOut;
import graphs.SymbolGraph;

public class Task4 {
	
	
	private List<String> starred_by(SymbolGraph sg, Graph G, String actor) {
		List<String> movies = new ArrayList<String>();
		if (sg.contains(actor)) {
            int s = sg.index(actor);
            for (int v : G.adj(s)) {
                movies.add(sg.name(v));
            }
        }
		return movies;
		
	}
	
	private void print(List<String> movies) {
		for (String movie : movies) {
			System.out.println(movie);
		}
	}
	
	public void run() {

		/*
		Write a program that finds the movies starred by a particular actor. Show the movies starred
		by Leonardo DiCaprio. Show the movies starred by Julia Roberts, by Hugh Grant, and by
		both of them
		 */
		String input_filename = System.getProperty("user.dir") + "/doc/movies.txt";
		long startTime, endTime;
		List<String> leonardoMovies, juliaMovies, hughMovies;
		startTime = System.currentTimeMillis();
		SymbolGraph sg = new SymbolGraph(input_filename, "/");
		Graph G = sg.G();
		String actor;
		
		actor = "DiCaprio, Leonardo";
		System.out.println("Movies starred by " + actor + ":");
		leonardoMovies = this.starred_by(sg, G, actor);
		this.print(leonardoMovies);
		System.out.println("");
		
		
		actor = "Roberts, Julia (I)";
		System.out.println("Movies starred by " + actor + ":");
		juliaMovies = this.starred_by(sg, G, actor);
		this.print(juliaMovies);
		System.out.println("");
		
		actor = "Grant, Hugh (I)";
		System.out.println("Movies starred by " + actor + ":");
		hughMovies = this.starred_by(sg, G, actor);
		this.print(hughMovies);
		System.out.println("");
		
		System.out.println("Movies starred by Roberts, Julia (I) & Grant, Hugh (I)");
		for (String movie : juliaMovies) {
			if (hughMovies.contains(movie)) {
				System.out.println(movie);
			}
		}
		
		endTime = System.currentTimeMillis() - startTime;
		System.out.println("Total time was " + endTime + " miliseconds ");
		
	}

}
