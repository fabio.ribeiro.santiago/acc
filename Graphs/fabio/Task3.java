package fabio;

import graphs.CC;
import graphs.Graph;
import graphs.StdOut;
import graphs.SymbolGraph;

public class Task3 {
	
	public void run() {
		long startTime, endTime;
		
		String input_filename = System.getProperty("user.dir") + "/doc/movies.txt";
		
		startTime = System.currentTimeMillis();
		SymbolGraph sg = new SymbolGraph(input_filename, "/");
		Graph G = sg.G();
		CC cc = new CC(G);
		int M = cc.count();
		endTime = System.currentTimeMillis() - startTime;
		
		System.out.println("Filename used: " + input_filename);
		System.out.println("The graph has " + M + " components");
		System.out.println("Total time: " + endTime + " miliseconds ");
		System.out.println("Complexity of CC constructor worst case: Proportional to O(V + E)");
		System.out.println("Complexity to count the number of components: Constant time");
	}

}
