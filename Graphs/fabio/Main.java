package fabio;

public class Main {

	public static void main(String[] args) {
		System.out.println("============================ Task 1 ============================");
		new Task1().run();
		System.out.println("");
		
		System.out.println("============================ Task 2 ============================");
		new Task2().run();
		System.out.println("");
		
		System.out.println("============================ Task 3 ============================");
		new Task3().run();
		System.out.println("");
		
		System.out.println("============================ Task 4 ============================");
		new Task4().run();
		System.out.println("");
	}

}
