import pandas
import matplotlib.pyplot as plt

filename = 'hash_times.csv'
df = pandas.read_csv(filename, delimiter=';',index_col=0,usecols=[0,1,3,5],header=0)
plot = df.plot(title='Insert experiment', lw=2, colormap='jet', marker='.', markersize=10)
plot.set_xlabel("Number of inserts")
plot.set_ylabel("AVG time in nanoseconds")
plt.show()