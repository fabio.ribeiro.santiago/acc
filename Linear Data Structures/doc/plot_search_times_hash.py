import pandas
import matplotlib.pyplot as plt

filename = 'hash_times.csv'
df = pandas.read_csv(filename, delimiter=';',index_col=0,usecols=[0,2,4,6],header=0)
plot = df.plot(title='Search and delete experiment', lw=2, colormap='jet', marker='.', markersize=10)
plot.set_xlabel("Number of search/delete operations")
plot.set_ylabel("AVG time in nanoseconds")
plt.show()