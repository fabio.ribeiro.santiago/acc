package fabio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

class CsvHashWriter {
	private PrintWriter pw;

	public CsvHashWriter(String filename) {
		try {
			this.pw = new PrintWriter(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/*
	 
	 The columns of CSV file will be
	 ==================================================
	 n;t1;t2;t3;t4;t5;t6
	 ==================================================
	 Where for each n number of elements
	 t1 = insert time for Cuckoo 
	 t2 = search + delete times for Cuckoo 
	 t3 = insert time for QuadraticProbing
	 t5 = search and delete times for QuadraticProbing 
	 t4 = insert time for SeparateChaining
	 t6 = search and delete times for SeparateChaining
	 
	 */
	
	public void writeHeader(String h) {
		pw.write(h+'\n');
	}
	
	public void writeMatrix(double[][] timeTable) {
		StringBuilder sb;
		for (int a = 0; a < timeTable.length; a++) {
			sb = new StringBuilder();
		    for (int b = 0; b < timeTable[a].length; b++) {
		    	sb.append(timeTable[a][b] + ";");
		    }
		    sb.append('\n');
		    pw.write(sb.toString());
		}
	}

	public void close() {
		this.pw.close();
	}
}