package fabio;

import hashTable.CuckooHashTable;
import hashTable.QuadraticProbingHashTable;
import hashTable.SeparateChainingHashTable;
import hashTable.StringHashFamily;

public class Main {
	
	public static double insertCukoo(CuckooHashTable<String> obj, String[] insertList) {
		long startTime = System.nanoTime();
		for (int i=0;i<insertList.length;i++) {
			obj.insert(insertList[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return (double) totalTime/insertList.length;
	}
	
	public static double searchAndDeleteCukoo(CuckooHashTable<String> obj, String[] list) {
		long startTime = System.nanoTime();
		for (int i=0;i<list.length;i++) {
			if(obj.contains(list[i])) {
				obj.remove(list[i]);
			}
		}
		long totalTime = System.nanoTime() - startTime;
		return (double) totalTime/list.length;
	}
	
	public static double insertQuadratic(QuadraticProbingHashTable<String> obj, String[] insertList) {
		long startTime = System.nanoTime();
		for (int i=0;i<insertList.length;i++) {
			obj.insert(insertList[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return (double) totalTime/insertList.length;
	}
	
	public static double searchAndDeleteQuadratic(QuadraticProbingHashTable<String> obj, String[] list) {
		long startTime = System.nanoTime();
		for (int i=0;i<list.length;i++) {
			if(obj.contains(list[i])) {
				obj.remove(list[i]);
			}
		}
		long totalTime = System.nanoTime() - startTime;
		return (double) totalTime/list.length;
	}
	
	public static double insertSeparateChanging(SeparateChainingHashTable<String> obj, String[] insertList) {
		long startTime = System.nanoTime();
		for (int i=0;i<insertList.length;i++) {
			obj.insert(insertList[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return (double) totalTime/insertList.length;
	}
	
	public static double searchAndDeleteSeparateChanging(SeparateChainingHashTable<String> obj, String[] list) {
		long startTime = System.nanoTime();
		for (int i=0;i<list.length;i++) {
			if(obj.contains(list[i])) {
				obj.remove(list[i]);
			}
		}
		long totalTime = System.nanoTime() - startTime;
		return (double) totalTime/list.length;
	}
	
	
	public static void main(String[] args) {
		// Hash data structures to be tested
		CuckooHashTable<String> cht;
		SeparateChainingHashTable<String> scht;
		QuadraticProbingHashTable<String> qpht;
		
		RandomString rs = new RandomString();
		// List of strings to be inserted in each data structure.
		String[] insertList;

		// List of strings to be searched and deleted in each data structure.
		String[] searchAndDeleteList;
		// Number of elements
		int n;
		// Number of loops
		int nl=20;
		// Table to collect the times of operations n;t1;t2;t3;t4;t5;t6
		double[][] timeTable = new double[nl][7];
		
		for (int i=1;i<=nl;i++) {
			n = (int) Math.pow(2, i);
			timeTable[i-1][0] = n; 
			// Generate a list of elements to be inserted;
			insertList = rs.getListOfStrings(n);
			
			// Generate a list of elements to be used in search and delete;
			searchAndDeleteList = rs.getListOfStrings(n);
			
			System.out.println("n="+n);
			
			//Cuckoo times t1 and t2
			System.out.println("Execunting Cuckoo Test...");
			cht = new CuckooHashTable<>( new StringHashFamily( 3 ), 2000);
			timeTable[i-1][1] = insertCukoo(cht, insertList);
			timeTable[i-1][2] = searchAndDeleteCukoo(cht, searchAndDeleteList);
			cht = null;
			
			//Quadratic times t3 and t4
			System.out.println("Execunting QuadraticProbing Test...");
			qpht = new QuadraticProbingHashTable<>( );
			timeTable[i-1][3] = insertQuadratic(qpht, insertList);
			timeTable[i-1][4] = searchAndDeleteQuadratic(qpht, searchAndDeleteList);
			qpht = null;
			
			//SeparateChanging times t5 and t6
			System.out.println("Execunting SeparateChaining Test...");
			scht = new SeparateChainingHashTable<>( );
			timeTable[i-1][5] = insertSeparateChanging(scht, insertList);
			timeTable[i-1][6] = searchAndDeleteSeparateChanging(scht, searchAndDeleteList);
			scht = null;
		}
		
		String filename = System.getProperty("user.dir") + "/doc/hash_times.csv";
		try {
			CsvHashWriter csvHW = new CsvHashWriter(filename);
			String header = "n;Insert avg time for Cuckoo;Search and delete avg time for Cuckoo;"
					+ "Insert avg time for Quadratic Probing;Search and delete avg time for Quadratic Probing;"
					+ "Insert avg time for Separate Changing;Search and delete avg time for Separate Changing;";
			csvHW.writeHeader(header);
			csvHW.writeMatrix(timeTable);
			csvHW.close();
			System.out.println(filename + " created");
		} catch (Exception e) {
			System.out.println("Can not create file in " + filename);
			e.printStackTrace();
		}
		
		/*
		// Print time tables
		for (int a = 0; a < timeTable.length; a++) {
		    for (int b = 0; b < timeTable[a].length; b++) {
		        System.out.print(timeTable[a][b] + " ");
		    }
		    System.out.println();
		}
		*/
	}
	
	
	
	

}
