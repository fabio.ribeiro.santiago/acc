/*
 * Code adapted from [https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string]
 *  
 */

package fabio;

import java.util.Random;

public class RandomString {

    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase();

    public static final String digits = "0123456789";
    
    public static final String charSet = upper + lower + digits;

    private final Random random = new Random();
    
    private final int stringLength = 10;

    private final char[] buf = new char[stringLength];
    
    private String getRandomString() {
    	char[] charSetArray = charSet.toCharArray();
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = charSetArray[random.nextInt(charSetArray.length)];
        return new String(buf);
    }
    
    /**
     * Generates an array of random Strings 
     * <p>
     *
     * @param  max  Size of array
     * @return      An array of random Strings
     */
    public String[] getListOfStrings(int max) {
    	String[] list = new String[max];
    	for (int i=0; i<max;i++) {
    		list[i]=getRandomString();
    	}
    	return list;
    }
 

  
}