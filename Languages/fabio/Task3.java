package fabio;

import java.io.*;
import org.jsoup.*;



public class Task3 {
	
	
	public void run() {
		
		System.out.println("=====================================");
		System.out.println("               Task 3                ");
		System.out.println("=====================================");
		
		File file;
		org.jsoup.nodes.Document doc;
		String textContent;
		
		String html_folder_path = System.getProperty("user.dir") + "/doc/html/";
		String txt_folder_path = System.getProperty("user.dir") + "/doc/txt/";
		String output_filename;
		
		File dir = new File(html_folder_path);
		File[] files = dir.listFiles();
		PrintWriter writer;
		
	    for (File f : files) {
	    	if (f.isFile()) {
	    		try {
					doc = Jsoup.parse(f, "UTF-8");
					textContent = doc.text();
					output_filename = txt_folder_path + f.getName() + ".txt";
					System.out.println("Writing " + output_filename);
					writer = new PrintWriter(output_filename, "UTF-8");
					writer.write(textContent);
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	}
	    }
	}
}
