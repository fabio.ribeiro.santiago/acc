package fabio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.*;

import org.jsoup.Jsoup;

public class Task4 {

	//String phone_pattern = "\\+?1?(\\d{10}|\\(\\d{3}\\)\\s?\\d{3}[-\\s]?\\d{4}|\\(\\d{3}\\)\\d{3}[-\\s]?\\d{4}|[\\s-]?\\d{3}[\\s-]?\\d{3}[\\s-]?\\d{4})";
	
	String phonePattern = "" 
		+ "("
			+ "(\\+1[\\-\\s]?|1[\\-\\s]?|1?)"
			+ "("
				+ "[1-9]{1}\\d{9}|"
				+ "\\([1-9]{1}\\d{2}\\)[\\-\\s]?\\d{3}[\\-\\s]?\\d{4}|"
				+ "[1-9]{1}\\d{2}[\\-\\s]?\\d{3}[\\-\\s]?\\d{4}"
			+ ")"
		+ ")";
	String emailPattern = "[a-zA-Z]+[a-zA-Z0-9\\-\\_\\.]*\\@[a-zA-Z\\-\\_\\.]+\\.[a-zA-Z]{2,}";

	public void test() {

		String[] phone_testStrings = {
				/* Valid phone number examples */
				"(023)4567890", "1234567890", "123-456-7890", "(123)456-7890", "(123)456 7890", "(123) 456-7890",
				"+1 (123) 456-7890", "+1 123 456 7890", "+11234567890", "11234567890", "123 456 7890", "+1 123 4567890",
				"+1 123-456-7890", "123-456 7890", "+1-123 456-7890", "=============================",
				/* Invalid phone number examples */
				"(1234567890)", "123)4567890", "(1)234567890", "21234567890", "+21234567890", "(123)-4567890", "1",
				"12-3456-7890", "123-4567", "Hello world", "(1234567890", "(123 456 7890", "+2 123 234 1234",
				"2-123-234 1234", "-123-234 1234", "-2123-234  1234", "10000000000", "1(000)234 4345", "045 321 1242"

		};

		for (String inputString : phone_testStrings) {
			System.out.print(inputString + ": ");
			if (inputString.matches(this.phonePattern)) {
				System.out.println("Valid");
			} else {
				System.out.println("Invalid");
			}
		}

		String[] email_testString = {
				// Valid email examples
				"fabio@gmail.com", "fabio.ribeiro@ab.com", "fabio_22_fs@gmail.ca",
				"FABIO.22_fs@gmail.ca", "f123@gmail.ca", "f-ribeiro@abc.us", "f-ribeiro@abc.com.br",
				"fabiors_ce@yahoo.com.br", "f@gmail-local.com",
				// Invalid email examples
				"====================",
				"f 123@gmail.ca", "2fa3@gmail.ca", "213@gmail", 
				"f-it@gmail.", "f-it@gmail.c", "fabio@123", "fabio@A23"
				 
		};
		// Apply regular expression to each test string
		for (String inputString : email_testString) {
			System.out.print(inputString + ": ");
			if (inputString.matches(this.emailPattern)) {
				System.out.println("Valid");
			} else {
				System.out.println("Invalid");
			}
		}

	}

	public void run() {
		
		System.out.println("=====================================");
		System.out.println("               Task 4                ");
		System.out.println("=====================================");
		
		
		String txtInputFolder = System.getProperty("user.dir") + "/doc/txt/";
		
		File dir = new File(txtInputFolder);
		File[] files = dir.listFiles();
		In fileReader;
		String textContent;
		boolean firstFind;
		Pattern compiledPattern;
		Matcher matcher;
		String phoneResultFile = System.getProperty("user.dir") + "/doc/task4_phone_result.txt";
		String emailResultFile = System.getProperty("user.dir") + "/doc/task4_email_result.txt";
		BufferedWriter writer;

		// compile phone pattern
		compiledPattern = Pattern.compile(this.phonePattern);
		System.out.println("Processing phone number pattern...");
		try {
			writer = new BufferedWriter(new FileWriter(phoneResultFile));
			// Loop for phone patterns
			for (File f : files) {
				if (f.isFile()) {
					fileReader = new In(f);
					textContent = fileReader.readAll();
					matcher = compiledPattern.matcher(textContent);
					firstFind = true;
						while (matcher.find()) {
							if (firstFind) {
								writer.write("Pattern found in file:" + f );
								writer.newLine();
								firstFind = false;
							}
							writer.write("Found value: " + matcher.group(0) + " at " + matcher.start(0));
							writer.newLine();
						}
				}
			}
			writer.close();
			System.out.println("Phone result file: " + phoneResultFile);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	
		// compile phone pattern
		compiledPattern = Pattern.compile(this.emailPattern);
		System.out.println("Processing email pattern...");
		try {
			writer = new BufferedWriter(new FileWriter(emailResultFile));
			// Loop for phone patterns
			for (File f : files) {
				if (f.isFile()) {
					fileReader = new In(f);
					textContent = fileReader.readAll();
					matcher = compiledPattern.matcher(textContent);
					firstFind = true;
						while (matcher.find()) {
							if (firstFind) {
								writer.write("Pattern found in file:" + f );
								writer.newLine();
								firstFind = false;
							}
							writer.write("Found value: " + matcher.group(0) + " at " + matcher.start(0));
							writer.newLine();
						}
				}
			}
			writer.close();
			System.out.println("Email result file: " + emailResultFile);
		} catch (IOException e1) {
			e1.printStackTrace();
		}


	}

}
