package fabio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task5 {
	
	private void searchForPattern(String pattern, String resultFile) {
		
		String txtInputFolder = System.getProperty("user.dir") + "/doc/txt/";
		File dir = new File(txtInputFolder);
		File[] files = dir.listFiles();
		BufferedWriter writer;
		In fileReader;
		String textContent;
		boolean firstFind;
		Pattern compiledPattern;
		Matcher matcher;
		
		compiledPattern = Pattern.compile(pattern);
		try {
			writer = new BufferedWriter(new FileWriter(resultFile));
			// Loop for phone patterns
			for (File f : files) {
				if (f.isFile()) {
					fileReader = new In(f);
					textContent = fileReader.readAll();
					matcher = compiledPattern.matcher(textContent);
					firstFind = true;
						while (matcher.find()) {
							if (firstFind) {
								writer.write("Pattern found in file:" + f );
								writer.newLine();
								firstFind = false;
							}
							writer.write("Found value: " + matcher.group(0) + " at " + matcher.start(0));
							writer.newLine();
						}
				}
			}
			writer.close();
			System.out.println("Result file: " + resultFile);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		
	}
	
	private void task5a() {
		System.out.println("Running task 5a...");
		//String pattern = "(http[s]?://)+[a-zA-Z0-9\\-\\_\\.]*\\.w3.org";
		String pattern = "(http[s]?://)+[a-zA-Z0-9\\-\\_\\.]*\\.w3.org([a-zA-Z0-9\\-\\_\\/\\%\\#]*|.[a-zA-Z0-9\\-\\_\\/\\%\\#]+)";
		String resultFile = System.getProperty("user.dir") + "/doc/task5a_result.txt";
		searchForPattern(pattern, resultFile);
	}
	
	private void task5b() {
		System.out.println("Running task 5b...");
		//String pattern = "(http[s]?://)+[a-zA-Z0-9\\-\\_\\.]*\\.[a-zA-Z]{2,}[\\/]+[a-zA-Z0-9\\-\\_\\.\\/\\%\\#]*[a-zA-Z0-9\\%\\#\\-\\_\\/]+";
		String pattern = "(http[s]?://)+[a-zA-Z0-9\\-\\_\\.]*\\.[a-zA-Z]{2,}[\\/]+([a-zA-Z0-9\\-\\_\\/\\%\\#]*|.[a-zA-Z0-9\\-\\_\\/\\%\\#]+)";
		String resultFile = System.getProperty("user.dir") + "/doc/task5b_result.txt";
		searchForPattern(pattern, resultFile);
	}
	
	private void task5c() {
		System.out.println("Running task 5c...");
		String pattern = "(http[s]?://)+[a-zA-Z0-9\\-\\_\\.]*\\.[a-zA-Z]{2,}[\\/]+[a-zA-Z0-9\\-\\_\\.\\/\\%]*[#]+[a-zA-Z0-9\\%\\#\\-\\_\\/]*";
		String resultFile = System.getProperty("user.dir") + "/doc/task5c_result.txt";
		searchForPattern(pattern, resultFile);
	}
	
	private void task5d() {
		System.out.println("Running task 5d...");
		String pattern = "(http[s]?://)+([a-zA-Z0-9\\-\\_\\.]+)(\\.com|\\.net|\\.org)([a-zA-Z0-9\\-\\_\\/\\%\\#]*|.[a-zA-Z0-9\\-\\_\\/\\%\\#]+)";
		String resultFile = System.getProperty("user.dir") + "/doc/task5d_result.txt";
		searchForPattern(pattern, resultFile);
	}
	
	public void run() {
			
			System.out.println("=====================================");
			System.out.println("               Task 5                ");
			System.out.println("=====================================");
			
			this.task5a();
			this.task5b();
			this.task5c();
			this.task5d();
	}
			
			
}
