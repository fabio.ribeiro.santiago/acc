package fabio;

public class Main {

	public static void main(String[] args) {
		System.out.println("============================ Task 5 ============================");
		new Task5().run();
		System.out.println("");
		
		System.out.println("============================ Task 6 ============================");
		new Task6().run();
		System.out.println("");
	}

}
