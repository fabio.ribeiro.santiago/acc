package fabio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import memoryManagement.BTree;
import memoryManagement.In;


public class Task6 {
	
	public void run() {
		
		long startTime = System.currentTimeMillis();
		String input_filename = System.getProperty("user.dir") + "/doc/ChIP-seq-reads-1M.dat"; 
		
		// Open file and read all lines
		System.out.println("Reading " + input_filename + "...");
		In input_file = new In(input_filename);
		String[] lines = input_file.readAllLines();
		
		BTree<String, String> st = new BTree<String, String>();
		PrintWriter pw;

		// Insert elements in Btree
		System.out.println("Inserting into Btree...");
		for (int i=0; i<lines.length; i++) {
			st.put(lines[i], lines[i]);
		}
		
		// Print Btree in file
		String output_file = System.getProperty("user.dir") + "/doc/B-tree.dat";
		System.out.println("Printing Btree in file " + output_file + "...");
		try {
			//StdOut.println(st); 
			pw = new PrintWriter(new File(output_file));
			pw.print(st);
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Done.");
		System.out.println("Total time: " + (endTime - startTime) + " milliseconds ");

	}

}
