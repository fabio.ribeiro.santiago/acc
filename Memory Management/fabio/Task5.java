package fabio;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;

import memoryManagement.In;
import memoryManagement.Multiway;

public class Task5 {

	private void writeInFile(String filename, String[] list) {
		BufferedWriter outputWriter = null;
		try {
			outputWriter = new BufferedWriter(new FileWriter(filename));
			for (int i = 0; i < list.length; i++) {
				outputWriter.write(list[i]);
				outputWriter.newLine();
			}
			outputWriter.flush();
			outputWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void run() {
		
		long startTime = System.currentTimeMillis();

		// Ler um arquivo grande
		String input_filename = System.getProperty("user.dir") + "/doc/ChIP-seq-reads-1M.dat";
		In myfile = new In(input_filename);
		String[] A, B, C, D; // Subarrays to sort the data
		System.out.println("Reading " + input_filename + " file... ");
		String[] lines = myfile.readAllLines();
		myfile.close();
		int sizeOfEachSublist = lines.length / 4;
		

		String as_file = System.getProperty("user.dir") + "/doc/AS.dat";
		System.out.println("Creating " + as_file + " file...");
		A = new String[sizeOfEachSublist];
		System.arraycopy(lines, 0, A, 0, lines.length / 4);
		Arrays.sort(A);
		this.writeInFile(as_file, A);
		A = null;

		String bs_file = System.getProperty("user.dir") + "/doc/BS.dat";
		System.out.println("Creating " + bs_file + " file...");
		B = new String[sizeOfEachSublist];
		System.arraycopy(lines, sizeOfEachSublist, B, 0, sizeOfEachSublist);
		Arrays.sort(B);
		this.writeInFile(bs_file, B);
		B = null;

		String cs_file = System.getProperty("user.dir") + "/doc/CS.dat";
		System.out.println("Creating " + cs_file + " file...");
		C = new String[sizeOfEachSublist];
		System.arraycopy(lines, sizeOfEachSublist * 2, C, 0, sizeOfEachSublist);
		Arrays.sort(C);
		this.writeInFile(cs_file, C);
		C=null;

		String ds_file = System.getProperty("user.dir") + "/doc/DS.dat";
		System.out.println("Creating " + ds_file + " file...");
		D = new String[sizeOfEachSublist];
		System.arraycopy(lines, sizeOfEachSublist * 3, D, 0, (lines.length - (3 * sizeOfEachSublist)));
		Arrays.sort(D);
		this.writeInFile(ds_file, D);
		D=null;
		
		
		System.out.println("Starting merge...");
		String[] fnames = {as_file, bs_file, cs_file, ds_file};
        int N = fnames.length; 
        In[] streams = new In[N]; 
        for (int i = 0; i < N; i++) 
            streams[i] = new In(fnames[i]);
        
		try {
			PrintStream console = System.out; 
			String merge_file = System.getProperty("user.dir") + "/doc/Chip-seq-reads-1M-sorted.dat";
			PrintStream fileStream = new PrintStream(merge_file);
			System.setOut(fileStream); 
	        Multiway.merge(streams); 
	        System.setOut(console);
	        fileStream.close();
	        System.out.println(merge_file + " created.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 

		long endTime = System.currentTimeMillis();
		System.out.println("Total time: " + (endTime - startTime) + " milliseconds.");
	}
}
