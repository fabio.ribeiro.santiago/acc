package fabio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

class CsvWriter {
	private PrintWriter pw;

	private void createFile(String filename) {
		try {
			this.pw = new PrintWriter(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void writeHeader(String h) {
		pw.write(h+'\n');
	}
	
	private void close() {
		this.pw.close();
	}
	
	public void writeTask2Results(long[][] timeTable) {
		
		String filename = System.getProperty("user.dir") + "/doc/task2_results.csv";
		System.out.println("Writting results in " + filename + " ...");
		this.createFile(filename);
		this.writeHeader("Interation;Mergesort;Quicksort;Heapsort;Dual-pivot Quicksort");
		
		StringBuilder sb;
		for (int a = 0; a < timeTable.length; a++) {
			sb = new StringBuilder();
			sb.append((a+1) + ";");
		    for (int b = 0; b < timeTable[a].length; b++) {
		    	sb.append(timeTable[a][b] + ";");
		    }
		    sb.append('\n');
		    pw.write(sb.toString());
		}
		this.close();
		
		System.out.println("Finished. Results are in " + filename + " file.");
		
	}
	
	public void writeTask3Results(long[][] timeTable, int sizeOfString) {
		
		System.out.println("Writting results for string size=" + sizeOfString);
		String filename = System.getProperty("user.dir") + "/doc/task3_strSize" + sizeOfString + "_results.csv";
		System.out.println("Writting results in " + filename + " ...");
		this.createFile(filename);
		this.writeHeader("Interation;Mergesort;Quicksort;Heapsort;Dual-pivot Quicksort;RadixSort");
		
		StringBuilder sb;
		for (int a = 0; a < timeTable.length; a++) {
			sb = new StringBuilder();
			sb.append((a+1)+ ";");
		    for (int b = 0; b < timeTable[a].length; b++) {
		    	sb.append(timeTable[a][b] + ";");
		    }
		    sb.append('\n');
		    pw.write(sb.toString());
		}
		this.close();
		
		System.out.println("Finished. Results are in " + filename + " file.");
		
	}
	
}