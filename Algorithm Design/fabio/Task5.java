package fabio;
import algorithmDesign.Sequences;

public class Task5 {
	
	public void run() {
		System.out.println("Running task5");
		int sizes[] = {10, 20, 50, 100};
		String s1;
		String s2;
		long t1, t2, totaltime;
		int dr;
		Sequences seq = new Sequences();
		
		RandomInput r = new RandomInput();
		for (int s=0; s<sizes.length; s++) {
			totaltime = 0;
			for (int i=0; i<1000; i++) {
				s1 = r.getRandomString(sizes[s]);
				s2 = r.getRandomString(sizes[s]);
				t1 = System.nanoTime();
				dr = seq.editDistance(s1, s2);
				t2 = System.nanoTime();
				totaltime += t2-t1;
			}
			System.out.println("Average time for 1000 random pairs of strings of size "+ sizes[s] + " was " + totaltime/1000 + " nano seconds.");
		}
	}

}
