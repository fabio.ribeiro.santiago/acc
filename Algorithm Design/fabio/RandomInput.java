package fabio;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomInput {
	
	private final Random random = new Random();
	
	public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase();

    public static final String charSet = upper + lower;

    public String getRandomString(int size) {
    	char[] buf = new char[size];
    	char[] charSetArray = charSet.toCharArray();
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = charSetArray[random.nextInt(charSetArray.length)];
        return new String(buf);
    }
}
