import pandas
import matplotlib.pyplot as plt

filename = 'task5_results.csv'
df = pandas.read_csv(filename, delimiter=';',index_col=0,usecols=[0,1],header=0)
plot = df.plot(title='Task5 experiment', lw=2, colormap='jet', marker='.', markersize=10)
plot.set_xlabel("String length")
plot.set_ylabel("Avg time in nano seconds")
figure = plt.gcf() # get current figure
figure.set_size_inches(8, 6)
plt.savefig('task5_chart.png', dpi=100)
#plt.show()
plt.close()
