package fabio;

import searchtrees.SplayTree;

public class SplayTreeTest {
	
	public long insertTest(SplayTree<Integer> tree, int[] list) {
		long startTime = System.nanoTime();
		for(int i=0; i<list.length;i++) {
			tree.insert(list[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}
	
	public long searchTest(SplayTree<Integer> tree, int[] list) {
		long startTime = System.nanoTime();
		for(int i=0; i<list.length;i++) {
			tree.contains(list[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}
	
	public long deleteTest(SplayTree<Integer> tree, int[] list, boolean checkBeforeDelete) {
		long startTime = System.nanoTime();
		if (checkBeforeDelete) {
			for(int i=0; i<list.length;i++) {
				if (tree.contains(list[i])) {
					tree.remove(list[i]);
				}
			}
		}
		else {
			for(int i=0; i<list.length;i++) {
				tree.remove(list[i]);
			}
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}

}
