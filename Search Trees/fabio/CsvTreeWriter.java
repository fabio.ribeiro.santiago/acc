package fabio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

class CsvTreeWriter {
	private PrintWriter pw;

	public CsvTreeWriter(String filename) {
		try {
			this.pw = new PrintWriter(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/*
	 
	 The columns of CSV file will be
	 ==================================================
	 tree;t1;t2;t3
	 ==================================================
	 Where:
	 tree = The kind of tree that is being tested.
	 t1 = insert time 
	 t2 = search time 
	 t3 = delete time
	 */
	
	public void writeHeader(String h) {
		pw.write(h+'\n');
	}
	
	public void writeMatrix(long[][] timeTable) {
		StringBuilder sb;
		for (int a = 0; a < timeTable.length; a++) {
			sb = new StringBuilder();
			switch (a) {
			case 0:
				sb.append("BinaryTree;");
				break;
			case 1:
				sb.append("AVLTree;");
				break;
			case 2:
				sb.append("RedBlackBST;");
				break;
			case 3:
				sb.append("SplayTree;");
				break;

			default:
				break;
			}
		    for (int b = 0; b < timeTable[a].length; b++) {
		    	sb.append(timeTable[a][b] + ";");
		    }
		    sb.append('\n');
		    pw.write(sb.toString());
		}
	}

	public void close() {
		this.pw.close();
	}
}