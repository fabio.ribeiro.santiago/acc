package fabio;

import java.util.Random;

import searchtrees.AVLTree;
import searchtrees.BinarySearchTree;
import searchtrees.RedBlackBST;
import searchtrees.SplayTree;

public class Main {
	
	public static void task4() {
		
		// Times for each tested tree
		long[][] times = new long[4][3];
		int[] insertList = new RandomInteger().getAscSortedListOfIntegers();
		int[] searchList = new RandomInteger().getRandomListOfIntegers();
		int[] deleteList = new RandomInteger().getDescSortedListOfIntegers();
		
		
		System.out.println("Executing Task 4 - Binary tree...");
		BinarySearchTree<Integer> binaryTree = new BinarySearchTree<>();
		times[0][0] = new BinaryTreeTest().insertTest(binaryTree, insertList);
		times[0][1] = new BinaryTreeTest().searchTest(binaryTree, searchList);
		times[0][2] = new BinaryTreeTest().deleteTest(binaryTree, deleteList, false);
		binaryTree = null;
		
		
		System.out.println("Executing Task 4 - AVL tree...");
		AVLTree<Integer> avlTree = new AVLTree<>();
		times[1][0] = new AVLTreeTest().insertTest(avlTree, insertList);
		times[1][1] = new AVLTreeTest().searchTest(avlTree, searchList);
		times[1][2] = new AVLTreeTest().deleteTest(avlTree, deleteList, false);
		avlTree = null;
		
		System.out.println("Executing Task 4 - RedBlackBST tree...");
		RedBlackBST<Integer, Integer> redBlackBst = new RedBlackBST<Integer, Integer>();
		times[2][0] = new RedBlackBSTTest().insertTest(redBlackBst, insertList);
		times[2][1] = new RedBlackBSTTest().searchTest(redBlackBst, searchList);
		times[2][2] = new RedBlackBSTTest().deleteTest(redBlackBst, deleteList, false);
		redBlackBst = null;
		
		System.out.println("Executing Task 4 - Splay tree...");
		SplayTree<Integer> splayTree = new SplayTree<Integer>( );
		times[3][0] = new SplayTreeTest().insertTest(splayTree, insertList);
		times[3][1] = new SplayTreeTest().searchTest(splayTree, searchList);
		times[3][2] = new SplayTreeTest().deleteTest(splayTree, deleteList, false);
		splayTree = null;
		
		String filename = System.getProperty("user.dir") + "/doc/task4.csv";
		try {
			CsvTreeWriter csv = new CsvTreeWriter(filename);
			String header = "tree;insert_time;search_time;delete_time";
			csv.writeHeader(header);
			csv.writeMatrix(times);
			csv.close();
			System.out.println(filename + " created");
		} catch (Exception e) {
			System.out.println("Can not create file in " + filename);
			e.printStackTrace();
		}
		
		
	}
	
	public static void task5() {
		
		// Times for each tested tree
		long[][] times = new long[4][3];
		int[] insertList = new RandomInteger().getRandomListOfIntegers();
		int[] searchList = new RandomInteger().getRandomListOfIntegers();
		int[] deleteList = new RandomInteger().getRandomListOfIntegers();
		
		
		System.out.println("Executing Task 5 - Binary tree...");
		BinarySearchTree<Integer> binaryTree = new BinarySearchTree<>();
		times[0][0] = new BinaryTreeTest().insertTest(binaryTree, insertList);
		times[0][1] = new BinaryTreeTest().searchTest(binaryTree, searchList);
		times[0][2] = new BinaryTreeTest().deleteTest(binaryTree, deleteList, true);
		binaryTree = null;
		
		
		System.out.println("Executing Task 5 - AVL tree...");
		AVLTree<Integer> avlTree = new AVLTree<>();
		times[1][0] = new AVLTreeTest().insertTest(avlTree, insertList);
		times[1][1] = new AVLTreeTest().searchTest(avlTree, searchList);
		times[1][2] = new AVLTreeTest().deleteTest(avlTree, deleteList, true);
		avlTree = null;
		
		System.out.println("Executing Task 5 - RedBlackBST tree...");
		RedBlackBST<Integer, Integer> redBlackBst = new RedBlackBST<Integer, Integer>();
		times[2][0] = new RedBlackBSTTest().insertTest(redBlackBst, insertList);
		times[2][1] = new RedBlackBSTTest().searchTest(redBlackBst, searchList);
		times[2][2] = new RedBlackBSTTest().deleteTest(redBlackBst, deleteList, true);
		redBlackBst = null;
		
		System.out.println("Executing Task 5 - Splay tree...");
		SplayTree<Integer> splayTree = new SplayTree<Integer>( );
		times[3][0] = new SplayTreeTest().insertTest(splayTree, insertList);
		times[3][1] = new SplayTreeTest().searchTest(splayTree, searchList);
		times[3][2] = new SplayTreeTest().deleteTest(splayTree, deleteList, true);
		splayTree = null;
		
		String filename = System.getProperty("user.dir") + "/doc/task5.csv";
		try {
			CsvTreeWriter csv = new CsvTreeWriter(filename);
			String header = "tree;insert_time;search_time;delete_time";
			csv.writeHeader(header);
			csv.writeMatrix(times);
			csv.close();
			System.out.println(filename + " created");
		} catch (Exception e) {
			System.out.println("Can not create file in " + filename);
			e.printStackTrace();
		}

		
	}

	public static void main(String[] args) {
		task4();
		task5();
	}

}
