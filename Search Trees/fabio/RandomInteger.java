package fabio;

import java.util.Random;

public class RandomInteger {
	
	private final Random random = new Random();
	
	private int listLength=100000;
	private int max=100000;
	

    /**
     * Generates an array of random Integers from 1 to max 
     * <p>
     *
     * @return      An array of random Integers
     */
	public int[] getRandomListOfIntegers() {
    	int[] list = new int[listLength];
    	for (int i=0; i<listLength;i++) {
    		list[i]=random.nextInt(this.max) + 1;
    	}
    	return list;
    }
	
	/**
     * Generates an asc ordered array of random Integers from 1 to max 
     * <p>
     *
     * @return      An array of random Integers
     */
	public int[] getAscSortedListOfIntegers() {
    	int[] list = new int[listLength];
    	for (int i=0; i<listLength;i++) {
    		list[i]=i+1;
    	}
    	return list;
    }
	
	/**
     * Generates an desc ordered array of random Integers from max to 1 
     * <p>
     *
     * @return      An array of random Integers
     */
	public int[] getDescSortedListOfIntegers() {
    	int[] list = new int[listLength];
    	for (int i=0; i<listLength;i++) {
    		list[i]=this.max-i;
    	}
    	return list;
    }
	
}
