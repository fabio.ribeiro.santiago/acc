package fabio;

import searchtrees.BinarySearchTree;

public class BinaryTreeTest {
	
	public long insertTest(BinarySearchTree<Integer> tree, int[] list) {
		long startTime = System.nanoTime();
		for(int i=0; i<list.length;i++) {
			tree.insert(list[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}
	
	public long searchTest(BinarySearchTree<Integer> tree, int[] list) {
		long startTime = System.nanoTime();
		for(int i=0; i<list.length;i++) {
			tree.contains(list[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}
	
	public long deleteTest(BinarySearchTree<Integer> tree, int[] list, boolean checkBeforeDelete) {
		long startTime = System.nanoTime();
		if (checkBeforeDelete) {
			for(int i=0; i<list.length;i++) {
				if (tree.contains(list[i])) {
					tree.remove(list[i]);
				}
			}
		}
		else {
			for(int i=0; i<list.length;i++) {
				tree.remove(list[i]);
			}
		}
				
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}

}
