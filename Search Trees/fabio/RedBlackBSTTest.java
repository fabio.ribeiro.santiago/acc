package fabio;

import searchtrees.RedBlackBST;

public class RedBlackBSTTest {
	
	public long insertTest(RedBlackBST<Integer, Integer> tree, int[] list) {
		long startTime = System.nanoTime();
		for(int i=0; i<list.length;i++) {
			tree.put(list[i], list[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}
	
	public long searchTest(RedBlackBST<Integer, Integer> tree, int[] list) {
		long startTime = System.nanoTime();
		for(int i=0; i<list.length;i++) {
			tree.contains(list[i]);
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}
	
	public long deleteTest(RedBlackBST<Integer, Integer> tree, int[] list, boolean checkBeforeDelete) {
		long startTime = System.nanoTime();
		if (checkBeforeDelete) {
			for(int i=0; i<list.length;i++) {
				if (tree.contains(list[i])) {
					tree.delete(list[i]);
				}
			}
		}
		else {
			for(int i=0; i<list.length;i++) {
				tree.delete(list[i]);
			}
		}
		long totalTime = System.nanoTime() - startTime;
		return totalTime/list.length;
	}

}
