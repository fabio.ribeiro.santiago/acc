package fabio;

import java.util.StringTokenizer;

import textprocessing.In;
import textprocessing.TST;

public class Task2 {
	
	private StringTokenizer tkob;
	private TST<Integer> st = new TST<Integer>();
	
	Task2() {
		// Read the file and get content
		String filename = System.getProperty("user.dir") + "/doc/Protein.txt";
		In in = new In(filename);
		String content =  in.readAll();
		in.close();
		
		// Get tokens using delimiters " .;,\n\r\t()"
		tkob = new StringTokenizer(content, " .;,\n\r\t()");
		
		String key;
		// Build a TST structure with the frequency of each word
		while (tkob.hasMoreTokens()) {
			key = tkob.nextToken();
			if (this.st.contains(key)) {
				this.st.put(key, this.st.get(key)+1);
			} else {
				this.st.put(key,1);
			}
		}
	}
	
	private void itemA() {
		System.out.println("=====================================");
		System.out.println("               Task 2a               ");
		System.out.println("=====================================");
		
		
		for (String k:this.st.keys()) {
			System.out.println("key=" + k + ", frequence="+ st.get(k));
		}
	}
	
	private void itemB() {
		System.out.println("=====================================");
		System.out.println("               Task 2b               ");
		System.out.println("=====================================");
		
		String[] words = {"protein", "complex", "PPI", "prediction", "quantity", "cancer", "fail", "hydrogen", "way" };
		for (String w:words) {
			System.out.println("Searching for " + w + "....");
			if (this.st.contains(w)) {
				System.out.println(w + " is present in text. Its frequency is " + st.get(w));
			} else {
				System.out.println(w + " is not present.");
			}
			
		}
	}
	
	public void run() {
		this.itemA();
		this.itemB();
	}

}
