package fabio;

import java.io.PrintStream;


import textprocessing.BoyerMoore;
import textprocessing.BruteForceMatch;
import textprocessing.In;
import textprocessing.KMP;

public class Task1 {
	
	String[] words = {"hard", "disk", "hard disk", "hard drive", "hard dist", "xltpru"};
	String content;
	
	// Contructor loads the file and read its content in content instance variable
	Task1() {
		String filename = System.getProperty("user.dir") + "/doc/Hard disk.txt";
		In in = new In(filename);
		this.content =  in.readAll();
		in.close();
	}
	
	private void bruteForce() {
		System.out.println("");
		System.out.println("Running brute force...");
		int absolutePosition, relativePosition;
		for (String word : this.words) {
			System.out.println("Searching for " + word + "... ");
			absolutePosition = 0;
			String txt = this.content;
			relativePosition = BruteForceMatch.search1(word, txt);
			if (relativePosition < txt.length()) {
				System.out.print(word + " is present in the text at the following positions: ");
			} else {
				System.out.print(word + " is not present in the text.");
			}
			while (relativePosition < txt.length()) {
				absolutePosition += relativePosition;
				System.out.print(absolutePosition + " ");
				absolutePosition += word.length();
				txt = txt.substring(relativePosition+word.length(), txt.length());
				relativePosition = BruteForceMatch.search1(word, txt);
			}
			System.out.println("");
		}
	}
	
	private void boyerMoore() {
		System.out.println("");
		System.out.println("Running Boyer Moore...");
		int absolutePosition, relativePosition;
		BoyerMoore bmObj;
		for (String word : this.words) {
			System.out.println("Searching for " + word + "... ");
			absolutePosition = 0;
			String txt = this.content;
			bmObj = new BoyerMoore(word);
			relativePosition = bmObj.search(txt);
			if (relativePosition < txt.length()) {
				System.out.print(word + " is present in the text at the following positions: ");
			} else {
				System.out.print(word + " is not present in the text.");
			}
			while (relativePosition < txt.length()) {
				absolutePosition += relativePosition;
				System.out.print(absolutePosition + " ");
				absolutePosition += word.length();
				txt = txt.substring(relativePosition+word.length(), txt.length());
				relativePosition = bmObj.search(txt);
				//System.out.println(txt);
			}
			System.out.println("");
		}
	}
	
	private void kmp() {
		System.out.println("");
		System.out.println("Running KMP...");
		int absolutePosition, relativePosition;
		KMP kmpObj;
		for (String word : this.words) {
			System.out.println("Searching for " + word + "... ");
			absolutePosition = 0;
			String txt = this.content;
			kmpObj = new KMP(word);
			relativePosition = kmpObj.search(txt);
			if (relativePosition < txt.length()) {
				System.out.print(word + " is present in the text at the following positions: ");
			} else {
				System.out.print(word + " is not present in the text.");
			}
			while (relativePosition < txt.length()) {
				absolutePosition += relativePosition;
				System.out.print(absolutePosition + " ");
				absolutePosition += word.length();
				txt = txt.substring(relativePosition+word.length(), txt.length());
				relativePosition = kmpObj.search(txt);
			}
			System.out.println("");
		}
	}
	
	private void itemB() {
		System.out.println("=====================================");
		System.out.println("               Task 1b               ");
		System.out.println("=====================================");
		this.bruteForce();
		this.boyerMoore();
		this.kmp();
	}
	
	
	private void itemC() {
		System.out.println("=====================================");
		System.out.println("         Tasks 1c and 1d             ");
		System.out.println("=====================================");
		
		long startTime, timeBruteForce, timeBoyerMoore, timeKMP;
		
		// Do not print messages
		PrintStream standard = System.out;
		System.setOut(new NullPrintStream());
		
		startTime = System.currentTimeMillis();
		for (int i=0; i<100; i++) {
			this.bruteForce();
		}
		timeBruteForce = System.currentTimeMillis() - startTime;
		
		startTime = System.currentTimeMillis();
		for (int i=0; i<100; i++) {
			this.boyerMoore();
		}
		timeBoyerMoore = System.currentTimeMillis() - startTime;
		
		startTime = System.currentTimeMillis();
		for (int i=0; i<100; i++) {
			this.kmp();
		}
		timeKMP = System.currentTimeMillis() - startTime;
		
		// Now, print the times
		System.setOut(standard);
		
		System.out.println("Brute Force cpu time: " + timeBruteForce + " milliseconds ");
		System.out.println("Brute Force complexity: O(nm)");
		System.out.println("Boyer Moore cpu time: " + timeBoyerMoore + " milliseconds ");
		System.out.println("Boyer Moore worst case complexity: O(nm + s), however, it is fast in average cases.");
		System.out.println("KMP cpu time: " + timeKMP + " milliseconds ");
		System.out.println("KMP optimal worst case complexity: O(m + n).");
		
	}
	
	public void run() {
		
		this.itemB();
		this.itemC();
		
	}

}
