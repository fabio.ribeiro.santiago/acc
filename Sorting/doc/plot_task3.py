import pandas
import matplotlib.pyplot as plt

strSizes = ['4','6','8','10']
for sSize in strSizes:
	filename = 'task3_strSize' + sSize + '_results.csv'
	df = pandas.read_csv(filename, delimiter=';',index_col=0,usecols=[0,1,2,3,4,5],header=0)
	plot = df.plot(title='Sort with string size=' + sSize + ' experiment', lw=2, colormap='jet', marker='.', markersize=10)
	plot.set_xlabel("Iteration")
	plot.set_ylabel("Sort time in microseconds")
	figure = plt.gcf() # get current figure
	figure.set_size_inches(8, 6)
	plt.savefig((sSize + '.png'),dpi=100)
	#plt.show()
	plt.close()

