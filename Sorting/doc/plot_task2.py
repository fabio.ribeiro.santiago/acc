import pandas
import matplotlib.pyplot as plt

filename = 'task2_results.csv'
df = pandas.read_csv(filename, delimiter=';',index_col=0,usecols=[0,1,2,3,4],header=0)
plot = df.plot(title='Task2 experiment', lw=2, colormap='jet', marker='.', markersize=10)
plot.set_xlabel("Interation")
plot.set_ylabel("Sort time in microseconds")
figure = plt.gcf() # get current figure
figure.set_size_inches(8, 6)
plt.savefig('task2_chart.png', dpi=100)
#plt.show()
plt.close()