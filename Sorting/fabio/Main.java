package fabio;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Running task 2...");
		new Task2().run();
		System.out.println("Task2 finished");
		
		System.out.println("Running task 3...");
		new Task3().run();
		System.out.println("Task3 finished");
		
	}

}
