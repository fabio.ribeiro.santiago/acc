package fabio;
import java.util.Arrays;

import sorting.Sort;

public class Task2 {
	
	private int numberOfRuns=100;
	
	public void run() {
		
		// Collects times for each 
		long[][] timeTable = new long[100][4]; 
		Long[] mergeSortList, quickSortList, heapSortList, dualPivotList;
		long startTime;
		long totalTime;
		
		for(int i=0; i < this.numberOfRuns; i++) {
			// We will use the same input for each algorithm. It is important to create a copy of the array.
			mergeSortList = new RandomInput().getRandomLongArray();
			quickSortList = mergeSortList.clone();
			heapSortList = mergeSortList.clone();
			dualPivotList = mergeSortList.clone();
			
			// MergeSort
			startTime = System.currentTimeMillis();
			Sort.mergeSort(mergeSortList);
			totalTime = System.currentTimeMillis() - startTime;
			timeTable[i][0] = totalTime;
			
			// QuickSort
			startTime = System.currentTimeMillis();
			Sort.quicksort(quickSortList);
			totalTime = System.currentTimeMillis() - startTime;
			timeTable[i][1] = totalTime;
			
			// HeapSort
			startTime = System.currentTimeMillis();
			Sort.heapsort(heapSortList);
			totalTime = System.currentTimeMillis() - startTime;
			timeTable[i][2] = totalTime;
			
			// Dual Pivot
			startTime = System.currentTimeMillis();
			Arrays.sort(dualPivotList);
			totalTime = System.currentTimeMillis() - startTime;
			timeTable[i][3] = totalTime;
		}
		
		
		new CsvWriter().writeTask2Results(timeTable);

		
	}

}
