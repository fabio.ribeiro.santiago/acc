package fabio;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomInput {
	
	private final Random random = new Random();
	
	private int listLength=100000;
	
	public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase();

    public static final String charSet = upper + lower;

    private String getRandomString(int size) {
    	char[] buf = new char[size];
    	char[] charSetArray = charSet.toCharArray();
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = charSetArray[random.nextInt(charSetArray.length)];
        return new String(buf);
    }
    
    /**
     * Generates an array of random Strings 
     * <p>
     *
     * @param  max  Size of array
     * @param  sizeOfString  String length
     * @return      An array of random Strings
     */
    public String[] getListOfStrings(int max, int sizeOfString) {
    	String[] list = new String[max];
    	for (int i=0; i<max;i++) {
    		list[i]=this.getRandomString(sizeOfString);
    	}
    	return list;
    }
	
    /**
     * Generates an array of random Long values 
     * <p>
     *
     * @return      An array of random Long values
     */
	public Long[] getRandomLongArray() {
		Long[] list=new Long[this.listLength];
		for (int i=0; i < this.listLength; i++) {
			list[i] = random.nextLong();
		}
		
		return list;
	}

}
