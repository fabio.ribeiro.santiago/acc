package fabio;

import java.util.Arrays;

import sorting.Sort;
import sorting.RadixSort;

public class Task3 {
	
	public void run() {
		RandomInput ri = new RandomInput();

		/*
		timeTables variable stores the times for each sort approach
			second dimension = 0..9 = number iteration
		 	third dimension = Sort method:
		 		0 = Merge Sort
		 		1 = Quick Sort
		 		2 = Heap Sort
		 		3 = Dual Pivot
		 		4 = Radix
		*/
		long[][] timeTable;
		
		String[] mergeSortList, quickSortList, heapSortList, dualPivotList, radixSortList;
		long startTime;
		long totalTime;
		int sizeOfString;

		for (int a=0; a < 4; a++) {
			
			sizeOfString = a*2+4;
			timeTable = new long[10][5];
			
			for (int i=0; i<10;i++) {
				mergeSortList = ri.getListOfStrings(100000, sizeOfString);
				quickSortList = mergeSortList.clone();
				heapSortList = mergeSortList.clone();
				dualPivotList = mergeSortList.clone();
				radixSortList = mergeSortList.clone();
				
				//System.currentTimeMillis()
				// MergeSort
				startTime = System.currentTimeMillis();
				Sort.mergeSort(mergeSortList);
				totalTime = System.currentTimeMillis() - startTime;
				timeTable[i][0] = totalTime;
				
				// QuickSort
				startTime = System.currentTimeMillis();
				Sort.quicksort(quickSortList);
				totalTime = System.currentTimeMillis() - startTime;
				timeTable[i][1] = totalTime;
				
				// HeapSort
				startTime = System.currentTimeMillis();
				Sort.heapsort(heapSortList);
				totalTime = System.currentTimeMillis() - startTime;
				timeTable[i][2] = totalTime;
				
				// Dual Pivot
				startTime = System.currentTimeMillis();
				Arrays.sort(dualPivotList);
				totalTime = System.currentTimeMillis() - startTime;
				timeTable[i][3] = totalTime;
				
				// Radix
				startTime = System.currentTimeMillis();
				RadixSort.radixSortA( radixSortList, sizeOfString );
				totalTime = System.currentTimeMillis() - startTime;
				timeTable[i][4] = totalTime;
				
			}
			
			new CsvWriter().writeTask3Results(timeTable, sizeOfString);
		}
		
	}

}
